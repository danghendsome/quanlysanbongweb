package Models;

public class Ban {
    public int idban;
    public String tenban;
    public int trangthaiban;

    //Constructor
    public Ban(int idban, String tenban, int trangthaiban) {
        this.idban = idban;
        this.tenban = tenban;
        this.trangthaiban = trangthaiban;
    }
    public  Ban(){

    }

//Getter và Setter
    public int getIdban() {
        return idban;
    }

    public void setIdban(int idban) {
        this.idban = idban;
    }

    public String getTenban() {
        return tenban;
    }

    public void setTenban(String tenban) {
        this.tenban = tenban;
    }

    public int getTrangthaiban() {
        return trangthaiban;
    }

    public void setTrangthaiban(int trangthaiban) {
        this.trangthaiban = trangthaiban;
    }
}
