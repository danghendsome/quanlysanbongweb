package Models;

public class ChiTietDvau {
    public int id;
    public int idHoaDon;
    public int idSanPham;
    public double soLuong;
    public double donGia;
    public double thanhTien;

    //Constructor
    public ChiTietDvau(int id, int idHoaDon, int idSanPham, double soLuong, double donGia, double thanhTien) {
        this.id = id;
        this.idHoaDon = idHoaDon;
        this.idSanPham = idSanPham;
        this.soLuong = soLuong;
        this.donGia = donGia;
        this.thanhTien = thanhTien;
    }

}

