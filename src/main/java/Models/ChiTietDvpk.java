package Models;

import java.time.LocalDateTime;

public class ChiTietDvpk {
    public int id;
    public int idHoaDon;
    public int idPhuKien;
    public double soLuong;
    public LocalDateTime gioMuon;
    public LocalDateTime gioTra;
    public double thanhTien;

    //Constructor
    public ChiTietDvpk(int id, int idHoaDon, int idPhuKien, double soLuong, LocalDateTime gioMuon, LocalDateTime gioTra, double thanhTien) {
        this.id = id;
        this.idHoaDon = idHoaDon;
        this.idPhuKien = idPhuKien;
        this.soLuong = soLuong;
        this.gioMuon = gioMuon;
        this.gioTra = gioTra;
        this.thanhTien = thanhTien;
    }
}

