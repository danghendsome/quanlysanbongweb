package Models;

import java.time.LocalDateTime;

public class ChiTietDvsan {
    public int id;
    public int idHoaDon;
    public int idSan;
    public LocalDateTime gioVao;
    public LocalDateTime gioRa;
    public double thanhTien;

    //Constructor
    public ChiTietDvsan(int id, int idHoaDon, int idSan, LocalDateTime gioVao, LocalDateTime gioRa, double thanhTien) {
        this.id = id;
        this.idHoaDon = idHoaDon;
        this.idSan = idSan;
        this.gioVao = gioVao;
        this.gioRa = gioRa;
        this.thanhTien = thanhTien;
    }
}
