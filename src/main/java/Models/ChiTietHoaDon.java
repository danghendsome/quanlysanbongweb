package Models;

public class ChiTietHoaDon {
    public int id;
    public int hoaDonId;
    public int productId;
    public float soLuong;
    public float donGia;

    public ChiTietHoaDon(int id, int hoaDonId, int productId, float soLuong, float donGia) {
        this.id = id;
        this.hoaDonId = hoaDonId;
        this.productId = productId;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }
}
