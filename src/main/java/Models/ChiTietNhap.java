package Models;

public class ChiTietNhap {
    public int id;
    public int idPhieuNhap;
    public int idSanPham;
    public double soLuong;
    public double gianhap;

    public ChiTietNhap(int id, int idPhieuNhap, int idSanPham, double soLuong, double gianhap) {
        this.id = id;
        this.idPhieuNhap = idPhieuNhap;
        this.idSanPham = idSanPham;
        this.soLuong = soLuong;
        this.gianhap = gianhap;
    }
}
