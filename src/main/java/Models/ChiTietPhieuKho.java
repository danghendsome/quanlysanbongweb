package Models;

public class ChiTietPhieuKho {
    public int id;
    public int idPhieu;
    public int idSanPham;
    public double soLuong;
    public String nguonNhap;

    public ChiTietPhieuKho(int id, int idPhieu, int idSanPham, double soLuong, String nguonNhap) {
        this.id = id;
        this.idPhieu = idPhieu;
        this.idSanPham = idSanPham;
        this.soLuong = soLuong;
        this.nguonNhap = nguonNhap;
    }

}
