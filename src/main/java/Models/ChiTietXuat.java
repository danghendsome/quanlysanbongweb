package Models;

public class ChiTietXuat {
    public int id;
    public int idPhieuXuat;
    public int idSanPham;
    public int soLuong;
    public int donGia;

    public ChiTietXuat(int id, int idPhieuXuat, int idSanPham, int soLuong, int donGia) {
        this.id = id;
        this.idPhieuXuat = idPhieuXuat;
        this.idSanPham = idSanPham;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }
}
