package Models;

import java.time.LocalDateTime;

public class DatPK {
    public int id;
    public int idPK;
    public int idKhach;
    public LocalDateTime gioVao;
    public LocalDateTime gioRa;
    public double datCoc;
}
