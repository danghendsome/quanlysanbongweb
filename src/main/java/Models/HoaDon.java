package Models;

import java.time.LocalDateTime;

public class HoaDon {
    public int id;
    public String code;
    public int idKhachHang;
    public int idNhanVien;
    public int idKhuVuc;
    public int idHinhThuc;
    public LocalDateTime gioVao;
    public LocalDateTime gioRa;
    public double tongTien;
}
