package Models;

import java.time.LocalDate;

public class NhanVien {
    public int id;
    public String tenNV;
    public LocalDate ngaySinh;
    public String diaChi;
    public char SDT;
}
