package Models;

import java.time.LocalDateTime;

public class PhieuNhap {
    public int idPhieuNhap;
    public LocalDateTime ngayNhap;
    public int idNhanVien;
    public String ghiChu;
}
