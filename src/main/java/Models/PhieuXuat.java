package Models;

import java.time.LocalDate;

public class PhieuXuat {
    public int idPhieuXuat;
    public LocalDate ngayXuat;
    public int idNhanVien;
    public String ghiChu;
}
