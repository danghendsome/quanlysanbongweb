package Models;

public class SanPham {

    public int idSanPham;
    public int idLoaiSanPham;
    public String tenSanPham;

    public SanPham() {
    }


    public SanPham(int idSanPham, int idLoaiSanPham, String tenSanPham){
        this.idSanPham=idSanPham;
        this.idLoaiSanPham=idLoaiSanPham;
        this.tenSanPham=tenSanPham;
    }

    //Getter và Setter
    public int getIdSanPham() {
        return idSanPham;
    }

    public void setIdSanPham(int idSanPham) {
        this.idSanPham = idSanPham;
    }

    public int getIdLoaiSanPham() {
        return idLoaiSanPham;
    }

    public void setIdLoaiSanPham(int idLoaiSanPham) {
        this.idLoaiSanPham = idLoaiSanPham;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }



}
