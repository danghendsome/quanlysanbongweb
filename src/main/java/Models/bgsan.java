package Models;

import java.time.LocalDate;
import java.time.LocalTime;

public class bgsan {
    public int id;
    public int idSan;
    public LocalDate ngayApDung;
    public String loaiSan;
    public LocalTime tuGio;
    public LocalTime denGio;
    public String donGia;

    //Constructor
    public bgsan(int id, int idSan, LocalDate ngayApDung, String loaiSan, LocalTime tuGio, LocalTime denGio, String donGia) {
        this.id = id;
        this.idSan = idSan;
        this.ngayApDung = ngayApDung;
        this.loaiSan = loaiSan;
        this.tuGio = tuGio;
        this.denGio = denGio;
        this.donGia = donGia;
    }

    //Getter và Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSan() {
        return idSan;
    }

    public void setIdSan(int idSan) {
        this.idSan = idSan;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getLoaiSan() {
        return loaiSan;
    }

    public void setLoaiSan(String loaiSan) {
        this.loaiSan = loaiSan;
    }

    public LocalTime getTuGio() {
        return tuGio;
    }

    public void setTuGio(LocalTime tuGio) {
        this.tuGio = tuGio;
    }

    public LocalTime getDenGio() {
        return denGio;
    }

    public void setDenGio(LocalTime denGio) {
        this.denGio = denGio;
    }

    public String getDonGia() {
        return donGia;
    }

    public void setDonGia(String donGia) {
        this.donGia = donGia;
    }
}
