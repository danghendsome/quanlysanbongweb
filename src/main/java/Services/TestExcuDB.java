package Services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import Models.SanPham;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TestExcuDB {
    private static Statement stmt;
    private static PreparedStatement stmt2;
    private static ResultSet results;

    public static void main(String[] args) {

        String sql_select = "Select * From sanpham";
        String timKiem ="Select* From sanpham where idsanpham=1";
//        String insert="INSERT INTO sanpham VALUES(9,9,'Hùng')";
        String update= "UPDATE sanpham SET tensanpham= 'Nước tăng lực M150' where idsanpham=8 ";
        ConnectDB cdb = new ConnectDB();
        try(Connection conn = cdb.createNewDBconnection()){

            //Sử dụng Statement
            stmt = conn.createStatement();
//            stmt.executeUpdate(insert);
//            System.out.println("Inserted records into the table...");
//            results = stmt.executeQuery(sql_select);
            results = stmt.executeQuery(timKiem);


            List<Models.SanPham> sanPhamList = new ArrayList<Models.SanPham>();
//            sanPhamList.add(new SanPham(13,13,"Kẹo côca"));
//            sanPhamList.add(new SanPham(14,14,"Nước Sting"));
//            sanPhamList.add(new SanPham(15,15,"Nước 7UP"));

            // create PreparedStatement to insert sanpham
//            String sql = "INSERT INTO sanpham (idsanpham, idLoaiSanPham, tenSanPham) VALUES (?, ?, ?);";
//            PreparedStatement stmt = conn.prepareStatement(sql);

//            //Update
//            String updating = "Update sanpham SET tensanpham = 'Kim Chi' WHERE idsanpham = 15";
//            stmt.executeUpdate(updating);
//            ResultSet results=stmt.executeQuery(sql_select);

            //Delete
            String deleted = "DELETE FROM sanpham WHERE idsanpham= 8";
            stmt.executeUpdate(deleted);
            ResultSet results=stmt.executeQuery(sql_select);

            // Dùng vòng lặp để in ra danh sách sản phẩm
//            for (SanPham sp : sanPhamList) {
//                stmt.setInt(1, sp.getIdSanPham());
//                stmt.setInt(2, sp.getIdLoaiSanPham());
//                stmt.setString(3, sp.getTenSanPham());
//                stmt.execute();
//            }

            while (results.next()) {

                Models.SanPham stdObject = new Models.SanPham();
// Sử dụng kết quả trả về
                stdObject.setIdSanPham(Integer.valueOf(results.getString("idsanpham")));
                stdObject.setIdLoaiSanPham(Integer.valueOf(results.getString("idloaisanpham")));
                stdObject.setTenSanPham(results.getString("tensanpham"));
                sanPhamList.add(stdObject);
            }

            ObjectMapper mapper = new ObjectMapper();
            String JSONOutput = mapper.writeValueAsString(sanPhamList);
            System.out.println(JSONOutput);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
